module gitlab.com/passelecasque/propolis

go 1.13

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/catastrophic/assistance v0.39.5
)

// replace gitlab.com/catastrophic/assistance => ../../catastrophic/assistance
// replace gitlab.com/passelecasque/obstruction => ../obstruction
